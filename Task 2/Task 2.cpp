#include <windows.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "clist.h"

void printNode(elemtype* value) {
    printf("%d\n", *((int*)value));
}

int main()
{
    system("chcp 1251");
    system("cls");

    cList* list = NULL;
    int choice, data, index;
    elemtype temp;
    int* newDataFront;
    int* newDataBack;
    int* newDataAtIndex;

    while (1) {
        printf("1. �������� ������ �� ������ ��������\n");
        printf("2. ������ ������� � ������� ������\n");
        printf("3. ������ ������� � ����� ������\n");
        printf("4. �������� ������� � ������� ������\n");
        printf("5. �������� ������� � ���� ������\n");
        printf("6. �������� ������� �� �������� ��������\n");
        printf("7. �������� ������� �� �������� ��������\n");
        printf("8. ������� ������ �� �����\n");
        printf("9. �������� ������\n");
        printf("10. ��������� ��������\n");
        printf("������� �����: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            if (list != NULL) {
                printf("������ ��� ����. ������ ���� ����� ���������� ������.\n");
                break;
            }
            list = createList();
            printf("������ �������� ��� ��������� � ������ (������ 0, ��� ��������� ��������):\n");
            while (1) {
                scanf_s("%d", &data);
                if (data == 0) {
                    break;
                }
                int* newData = (int*)malloc(sizeof(int));
                *newData = data;
                pushBack(list, newData);
            }
            break;
        case 2:
            if (list == NULL) {
                printf("�������� ������� ������.\n");
                break;
            }
            printf("������ ���� ����� ��� ���������: ");
            scanf_s("%d", &data);
            newDataFront = (int*)malloc(sizeof(int));
            *newDataFront = data;
            pushFront(list, newDataFront);
            break;
        case 3:
            if (list == NULL) {
                printf("�������� ������� ������.\n");
                break;
            }
            printf("������ ���� ����� ��� ���������: ");
            scanf_s("%d", &data);
            newDataBack = (int*)malloc(sizeof(int));
            *newDataBack = data;
            pushBack(list, newDataBack);
            break;
        case 4:
            if (list == NULL) {
                printf("�������� ������� ������.\n");
                break;
            }
            if (popFront(list, &temp) == 0) {
                printf("�������� ������� � ������� ������: %d\n", temp);
            }
            else {
                printf("������ ��������.\n");
            }
            break;
        case 5:
            if (list == NULL) {
                printf("�������� ������� ������.\n");
                break;
            }
            if (popBack(list, &temp) == 0) {
                printf("�������� ������� � ���� ������: %d\n", temp);
            }
            else {
                printf("������ ��������.\n");
            }
            break;
        case 6:
            if (list == NULL) {
                printf("�������� ������� ������.\n");
                break;
            }
            printf("������ ������ ��� �������: ");
            scanf_s("%d", &index);
            printf("������ ���� ����� ��� �������: ");
            scanf_s("%d", &data);
            newDataAtIndex = (int*)malloc(sizeof(int));
            *newDataAtIndex = data;
            if (insertAtIndex(list, newDataAtIndex, index) == 0) {
                printf("������� ������ ����������.\n");
            }
            else {
                printf("��������� �������� ������� �� �������� ��������.\n");
            }
            break;
        case 7:
            if (list == NULL) {
                printf("�������� ������� ������.\n");
                break;
            }
            printf("������ ������ ��� ���������: ");
            scanf_s("%d", &index);
            if (deleteAtIndex(list, index, &temp) == 0) {
                printf("�������� ������� � �������� %d: %d\n", index, temp);
            }
            else {
                printf("��������� �������� ������� �� �������� ��������.\n");
            }
            break;
        case 8:
            if (list == NULL) {
                printf("�������� ������� ������.\n");
                break;
            }
            printf("������: \n");
            printList(list, printNode);
            printf("\n");
            break;
        case 9:
            if (list != NULL) {
                deleteList(list);
                list = NULL;
            }
            printf("������ ��������.\n");
            break;
        case 10:
            printf("�������� ���������.\n");
            exit(0);
            break;
        default:
            printf("������� ����. ���� �����, ������� ����� �����.\n");
        }
    }

    return 0;
}