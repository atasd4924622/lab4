#include "clist.h"

// ��������� ���������� ������
cList* createList(void) {
    cList* list = (cList*)malloc(sizeof(cList));
    if (list) {
        list->size = 0;
        list->head = list->tail = NULL;
    }
    return list;
}

// ��������� ������
void deleteList(cList* list) {
    cNode* head = list->head;
    cNode* next = NULL;
    while (head) {
        next = head->next;
        free(head);
        head = next;
    }
    free(list);
    list = NULL;
}

// �������� ������ �� ���������
bool isEmptyList(cList* list) {
    return ((list->head == NULL) || (list->tail == NULL));
}

// ��������� ������ ����� �� ������� ������
int pushFront(cList* list, elemtype* data) {
    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node) {
        return (-1);
    }
    node->value = data;
    node->next = list->head;
    node->prev = NULL;

    if (!isEmptyList(list)) {
        list->head->prev = node;
    }
    else {
        list->tail = node;
    }
    list->head = node;

    list->size++;
    return (0);
}

// ��������� ����� � ������� ������
int popFront(cList* list, elemtype* data) {
    cNode* node;

    if (isEmptyList(list)) {
        return (-2);
    }

    node = list->head;
    list->head = list->head->next;

    if (!isEmptyList(list)) {
        list->head->prev = NULL;
    }
    else {
        list->tail = NULL;
    }

    *data = *(node->value);
    list->size--;
    free(node);

    return (0);
}

// ��������� ������ ����� � ����� ������
int pushBack(cList* list, elemtype* data) {
    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node) {
        return (-3);
    }

    node->value = data;
    node->next = NULL;
    node->prev = list->tail;
    if (!isEmptyList(list)) {
        list->tail->next = node;
    }
    else {
        list->head = node;
    }
    list->tail = node;

    list->size++;
    return (0);
}

// ����� ����� � ���� ������
int popBack(cList* list, elemtype* data) {
    cNode* node = NULL;

    if (isEmptyList(list)) {
        return (-4);
    }

    node = list->tail;
    list->tail = list->tail->prev;
    if (!isEmptyList(list)) {
        list->tail->next = NULL;
    }
    else {
        list->head = NULL;
    }

    *data = *(node->value);
    list->size--;
    free(node);

    return (0);
}

// ������� ��������� ����� ������
cNode* getNode(cList* list, int index) {
    cNode* node = NULL;
    int i;

    if (index >= list->size) {
        return (NULL);
    }

    if (index < list->size / 2) {
        i = 0;
        node = list->head;
        while (node && i < index) {
            node = node->next;
            i++;
        }
    }
    else {
        i = list->size - 1;
        node = list->tail;
        while (node && i > index) {
            node = node->prev;
            i--;
        }
    }

    return node;
}

// ������� ������ ����� �� �������� ��������
int insertAtIndex(cList* list, elemtype* data, int index) {
    if (index < 0 || index > list->size) {
        return -5; // �������: ������������ ������
    }

    if (index == 0) {
        return pushFront(list, data); // ������� � ������� ������
    }

    if (index == list->size) {
        return pushBack(list, data); // ������� � ����� ������
    }

    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node) {
        return -6; // �������: �� ������� ������� ���'��� ��� ������ �����
    }

    node->value = data;

    cNode* current = getNode(list, index);
    if (!current) {
        free(node);
        return -7; // �������: �� ������� ������ ����� �� �������� ��������
    }

    node->next = current;
    node->prev = current->prev;
    current->prev->next = node;
    current->prev = node;

    list->size++;
    return 0; // ������ �������
}

// ��������� ����� �� �������� ��������
int deleteAtIndex(cList* list, int index) {
    elemtype data;

    if (isEmptyList(list) || index < 0 || index >= list->size) {
        return -8;
    }

    if (index == 0) {
        return popFront(list, &data);
    }

    if (index == list->size - 1) {
        return popBack(list, &data);
    }

    cNode* current = getNode(list, index);
    if (!current) {
        return -9;
    }

    current->prev->next = current->next;
    current->next->prev = current->prev;
    data = *(current->value);
    free(current);

    list->size--;
    return 0;
}



// ��������� ������ � �������
void printList(cList* list, void (*func)(elemtype*)) {
    cNode* node = list->head;

    if (isEmptyList(list)) {
        return;
    }

    while (node) {
        func(node->value);
        node = node->next;
    }
}