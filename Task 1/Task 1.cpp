#include <windows.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "clist.h"

void printNode(elemtype* value) {
    printf("%d\n", *((int*)value));
}

int main() {

    system("chcp 1251");
    system("cls");

    elemtype a = 0;
    elemtype b = 1;
    elemtype c = 2;
    elemtype tmp;

    cList* mylist = createList();



    pushFront(mylist, &a);
    printf("������ ���� ��������� 'a' �� �������:\n");
    printList(mylist, printNode);
    printf("\n");

    popBack(mylist, &tmp);

    pushFront(mylist, &b);
    printf("������ ���� ��������� ���������� �������� �� ��������� 'b' �� �������:\n");
    printList(mylist, printNode);
    printf("\n");

    pushFront(mylist, &a);
    printf("������ ���� ��������� 'a' �� �������:\n");
    printList(mylist, printNode);
    printf("\n");

    pushBack(mylist, &c);
    printf("������ ���� ��������� 'c' � �����:\n");
    printList(mylist, printNode);
    printf("\n");

    printf("������ ���� ��������� ������� ��������:\n");
    popFront(mylist, &tmp);
    printList(mylist, printNode);
    printf("\n");

    pushBack(mylist, &a);
    printf("������ ���� ��������� 'a' � �����:\n");
    printList(mylist, printNode);
    printf("\n");

    pushFront(mylist, &a);
    printf("������ ���� ��������� 'a' �� �������:\n");
    printList(mylist, printNode);
    printf("\n");

    printf("�������� ����� �� �������� 3:\n");
    printNode(getNode(mylist, 3)->value);
    printf("\n");

    printf("��������� ����� 'c' �� �������� 3:\n");
    insertAtIndex(mylist, &c, 3);
    printList(mylist, printNode);
    printf("\n");

    printf("��������� ����� �� �������� 2:\n");
    deleteAtIndex(mylist, 2);
    printList(mylist, printNode);
    printf("\n");

    printf("ʳ������ ������:\n");
    printList(mylist, printNode);
    printf("\n");

    deleteList(mylist);

    system("pause");
    return 0;
}